package ir.coffeecode.numtoalpha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class actMain extends AppCompatActivity {

    String[] Yekan = {"صفر", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"};
    String[] Dahgan = {"", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"};
    String[] Sadgan;

    EditText txtNum;
    TextView txvAlpha;

    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_main);

        txtNum = (EditText) findViewById(R.id.txtNum);
        txvAlpha = (TextView) findViewById(R.id.txvAlpha);

        txtNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String strIndex = txtNum.getText().toString();
                if (!strIndex.equals("")) {
                    index = Integer.parseInt(strIndex);
                    int numOne = (int) Math.ceil(index / 10);
                    int numTwo = index % 10;
                    if (!Dahgan[numOne].equals(""))
                        txvAlpha.setText(Dahgan[numOne] + " و " + Yekan[numTwo]);
                    else
                        txvAlpha.setText(Yekan[numTwo]);
                }
                else
                    txvAlpha.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
